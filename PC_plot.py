import numpy as np
from numpy.lib.arraysetops import unique
import pandas as pd
from pandas.io.parsers import read_csv
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
import random
def random_color():
     colors1 = '0123456789ABCDEF'
     num = "#"
     for i in range(6):
         num += random.choice(colors1)
     return num
random.seed(0)
raw_data = pd.read_csv('PC.csv')
pids = pd.unique(raw_data['pid'])


fig, (ax1, ax2) = plt.subplots(2,1, sharex=True)#figsize=(8, 2))
# plt.subplot(2,1,1)
for pid in pids:
    kid = raw_data[raw_data['pid'] == pid]
    for idx, (id, row) in enumerate(kid.iterrows()):
        if row['action'].endswith('done'):
            # ax.barh(pid, row['tick']+0.02, left=row['tick'], color="black", height=.1)
            continue
        elif row['action'].endswith('try'):
            # from IPython import embed; embed()
            bar_left = row['tick']
            bar_right = int(kid[idx+1:idx+2]['tick'])
            ax1.barh(pid, width=bar_right-bar_left, left=bar_left, color=random_color(), height=.1)#, orientation='horizontal')
            ax1.text(bar_right, pid+0.1, str(bar_right), ha='center')
            if bar_right!=bar_left:
                barname = 'wait2eat' if row['action'].startswith(' eat') else 'wait2make'
                ax1.text((bar_left+bar_right)/2, pid, barname, ha='center')
        elif row['action'].endswith('stat'):
            bar_left = row['tick']
            bar_right = int(kid[idx+1:idx+2]['tick'])
            ax1.barh(pid, width=bar_right-bar_left, left=bar_left, color=random_color(), height=.1)#, orientation='horizontal')
            ax1.text(bar_right, pid+0.1, str(bar_right), ha='center')
            barname = 'eat' if row['action'].startswith(' eat') else 'make'
            ax1.text((bar_left+bar_right)/2, pid, barname, ha='center')

        else:
            raise ZeroDivisionError()
    
    

ax1.set_yticks(pids, [str(pid) for pid in pids])
ax1.set_title("Process Behavior", fontsize=15)
ax1.set_xlabel("Time")
ax1.set_ylabel("Process ID")


# plt.subplot(2,1,2)

food = 0
slot = 3
foods = [food,]
slots = [slot,]
ts1 = [0,]
ts2 = [0,]

for idx, (id, row) in enumerate(raw_data.iterrows()):
    if row['action'].endswith('done'):
        if row['action'].startswith(' make'):
            ts1.append(row['tick'])
            ts1.append(row['tick'])
            foods.append(food)
            food += 1
            foods.append(food)
        # ax.barh(pid, row['tick']+0.02, left=row['tick'], color="black", height=.1)
        continue
    elif row['action'].endswith('try'):
        if row['action'].startswith(' eat'):
            ts1.append(row['tick'])
            ts1.append(row['tick'])
            foods.append(food)
            food -= 1
            foods.append(food)
        elif row['action'].startswith(' make'):
            ts2.append(row['tick'])
            ts2.append(row['tick'])
            slots.append(slot)
            slot -= 1
            slots.append(slot)
    elif row['action'].endswith('stat'):
        if row['action'].startswith(' eat'):
            ts2.append(row['tick'])
            ts2.append(row['tick'])
            slots.append(slot)
            slot += 1
            slots.append(slot)
            
    else:
        raise ZeroDivisionError()
ax2.plot(ts1, foods, label='foods')
ax2.plot(ts2, slots, label='slots')
fig.align_xlabels(axs=[ax1, ax2])
ax2.set_title("Semaphor Behavior", fontsize=15)
ax2.set_xlabel("Time")
ax2.set_ylabel("Value")
plt.legend()
plt.show()